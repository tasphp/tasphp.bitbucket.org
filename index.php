<?php 
include("header.php");
?>
<div class="video">
	<video poster="" id="bgvid" playsinline autoplay muted loop class="background_img">
		<source src="assets/vidoes/globe-and-twinkling-stars-in-space_wjf5zs2ls__D.mp4" type="video/mp4">
		<?php /*<source src="assets/images/small.ogv" type="video/ogg">*/ ?>
	</video>
	<!-- <div class="overlay-desc">
		<h1>WE EXIST<br> TO MAKE A<br> DIFFERENCE</h1>
	</div> -->
</div>
<section class="divider">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1><p></p>WE EXIST TO MAKE A DIFFERENCE</h1>
      </div>
    </div>
  </div>
</section>
<section id="aboutus_area" class="aboutus_area clearfix">
  <div class="col-md-6 wow fadeInRightBig animated" data-wow-duration="1500ms" data-wow-delay="800ms">
      <img src="assets/images/aa.jpg" alt="" />
    </div>
    <div class="col-md-6 our_mission wow fadeInLeftBig animated" data-wow-duration="1500ms" data-wow-delay="700ms">
      <div class="container text-left big_font" style="padding: 10%;">
          <p>Sportspeople are not machines. For each triumphant moment, experience by some, there is always a sentiment of defeat felt by others.</p>
          <p>As sportspeople develop toward maturity, they need to know that someone cares for them and this is how we show compassion.</p>
          <p>We share the Christian faith and values, because sportspeople are often misguided about their worth and dignity. We show our devotion, when we point them to their true identity.</p>
          <p>Our experience allows us to mentor sportspeople to excel in every area of life.</p>
          <p>When we help sportspeople to perform with all their hearts, we are doing it with excellence.</p>
        </div>
    </div>
</section>

<div class="push"></div>
</div>
<?php 
include("footer.php");
?>